package ru.lanit.at.pages.optionals;

/**
 * Parent of optional interfaces. Optional page interfaces indicates that some page has specified option or capability.
 */
public interface OptionalPageInterface {
}
