package ru.lanit.at.pages.optionals;

public interface Openable extends OptionalPageInterface {
    boolean isOpened();
}
