package ru.lanit.at;

public class FrameworkConstants {

    public static final String ENABLE_VIDEO = "enableVideo";
    public static final String ENABLE_VNC = "enableVNC";
    public static final String REMOTE_DRIVER_VARIABLE_NAME = "remote";
    public static final String HUB_URL_VARIABLE_NAME = "hub_url";
    public static final String BROWSER_VARIABLE_NAME = "browser";
    public static final String PROXY_VARIABLE_NAME = "proxy";
    public static final String GECKO_DRIVER_PATH_VARIABLE_NAME = "webdriver.gecko.driver";
    public static final String CHROME_DRIVER_PATH_VARIABLE_NAME = "webdriver.chrome.driver";

    public static final String DEFAULT_HUB_URL = "http://localhost:4444/wd/hub";
    public static final int DEFAULT_TIMEOUT = 10; //The timeout in seconds
    public static final String DEFAULT_BROWSER = "chrome";
    public static final String DEFAULT_WINIUM_HUB_URL = "http://localhost:9999";
    public static final String DEFAULT_GECKO_DRIVER_PATH = "src/main/resources/drivers/geckodriver.exe";
    public static final String DEFAULT_CHROME_DRIVER_PATH = "src/main/resources/drivers/chromedriver.exe";
    public static final String DEFAULT_CHROME_CONFIG = "chromedriver.config.yaml";
    public static final String DEFAULT_GECKO_CONFIG = "geckodriver.config.yaml";
    public static final String DEFAULT_PROXY_CONFIG = "proxy.config.yaml";
    public static final String DEFAULT_TIMEOUTS_CONFIG = "timeouts.config.yaml";

    public static final String IMPLICITLY_WAIT = "implicitlyWait";
    public static final String PAGE_LOAD_TIMEOUT = "pageLoadTimeout";
    public static final String SCRIPT_TIMEOUT = "scriptTimeout";


}
